const Jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const { application } = require("../../Infrastructure/config");
const appError = require("../../../HTTP/errors/appError");
const AuthService = require("./authService");
const UserFactory = require("../../Infrastructure/factories/userFactory");
const store = UserFactory.getUserStore();

class JwtAuthService extends AuthService {

  static async validateUserCredentials(params) {
    const userIsPresent = await store.findByEmail(params.email);
    if (!userIsPresent) {
      throw new appError("Invalid Email.", 400);
    }

    if (!bcrypt.compare(params.password, userIsPresent.password)) {
      throw new appError("Invalid Password.", 400);
    }
    return { token: this.generateJwtToken(userIsPresent.userID) };
  }

  static generateJwtToken(userID) {
    return Jwt.sign({ userID: userID }, application.myPrivateKey);
  }

}
module.exports = JwtAuthService;